using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
* Gère les données utilisées dans les menus et dans le jeu.
*
*/
public class GameManager : MonoBehaviour
{   
    private static GameManager m_Instance;

    public static GameManager Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<GameManager>();
            }

            return m_Instance;
        }
    }

    List<Player> m_currentPlayers = new List<Player>();

    /// <summary>
    /// Liste des tanks finalistes
    /// </summary>
    public List<GameObject> FinalistTanks = new List<GameObject>();

    #region rules
    //*** Tanks    
    //*********

    /// <summary>
    /// Vie de départ des tanks
    /// </summary>
    public float TankHealth = 100f;

    /// <summary>
    /// Vitesse du tank
    /// </summary>
    public float TankSpeed = 10;

    //*** Armement
    //************

    /// <summary>
    /// Dégats de l'obus
    /// </summary>
    public int BulletDmg = 40;

    /// <summary>
    /// Puissance de recul de l'obus
    /// </summary>
    public int BulletRecoilPower = 300;

    /// <summary>
    /// Puissance de l'explosion au contact
    /// </summary>
    public int BulletExplosionPower = 100;

    /// <summary>
    /// Puissance du souffle de l'explosion vers le haut au contact
    /// </summary>
    public int BulletExplosionBlow = 50;

    /// <summary>
    /// Nombre de rebonds max de l'obus
    /// </summary>
    public int BulletRebound = 3;

    /// <summary>
    /// Temps minimum entre chaque tir
    /// </summary>
    public int BulletReload = 1;

    /// <summary>
    /// Nombre de mine au départ
    /// </summary>
    public int LandmineBaseNb = 0;

    /// <summary>
    /// Nombre de mine max
    /// </summary>
    public int LandmineMaxCapacity = 5;

    /// <summary>
    /// Temps entre chaque apparition
    /// de mines sur un spawner
    /// </summary>
    public int LandmineRespawn = 15;

    /// <summary>
    /// Dégats des mines
    /// </summary>
    public int LandmineDmg = 100;

    /// <summary>
    /// Portée de ramassage des mines
    /// </summary>
    public int LandmineLootRadius = 5;

    //*** Partie
    //**********

    /// <summary>
    /// Durée de la partie en minutes
    /// </summary>
    public int GameDuration = 5;

    /// <summary>
    /// Nombre de vies
    /// </summary>
    public int GameRespawnNb = 0;

    /// <summary>
    /// Nombre d'IA dans la partie
    /// </summary>
    public int GameBotNb = 0;

    #endregion

    public enum GameStatus
    {
        Stopped,
        Running,
        Win,
        Tie,
        TimeTie
    }

    public GameStatus CurrentStatus = GameManager.GameStatus.Stopped;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Initialize();
            GoToMainMenu();
        }
    }

    public void Initialize()
    {
        DontDestroyOnLoad(gameObject);
        m_currentPlayers.Clear();
        FinalistTanks.Clear();
    }

    public void AddPlayer(Player player)
    {
        m_currentPlayers.Add(player);
    }

    public void ClearPlayers()
    {
        m_currentPlayers.Clear();
    }

    public List<Player> GetPlayers()
    {
        return m_currentPlayers;
    }

    private void GoToMainMenu()
    {
        SceneManager.LoadScene("Scenes/Menus/MainMenu");
    }
}
