using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform m_Target;
    public float m_MoveSpeed = 5f;
    public float m_RotateSpeed = 10f;
    public float m_OffsetBehind = 8f;
    public float m_OffsetAbove = 8f;
    public float m_OffsetBeside = 0f;
    public float m_LookUpAngle = 18f;
    public float m_SmoothTime = 0.3F;

    private Vector3 m_velocity = Vector3.zero;

    private Vector3 m_Offset;

    void Start()
    {
        UpdateOffset();
    }

    void LateUpdate()
    {   
        if (m_Target != null)
        {
            // Positionnement de la caméra sur la cible
            gameObject.transform.position = m_Target.transform.position;
            gameObject.transform.rotation = m_Target.transform.rotation;

            // Prise en compte de l'offset, en général pour reculer
            transform.Translate(m_Offset, transform);
            
            // Calcul de la rotation pour viser le tank 
            transform.LookAt(m_Target.transform);
            transform.Rotate(new Vector3(-m_LookUpAngle, 0, 0));
        }
    }

    void UpdateOffset()
    {
        m_Offset = (m_Target.forward * -m_OffsetBehind) +
                    (m_Target.up * m_OffsetAbove) +
                    (m_Target.right * m_OffsetBeside);
    }

    public void SetTarget(Transform target)
    {
        m_Target = target;
    }
}
