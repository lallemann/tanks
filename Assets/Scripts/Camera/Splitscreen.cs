using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Splitscreen
{

    public const int MAX_WIDTH = 1;
    public const int MAX_HEIGHT = 1;

    /// <summary>
    /// Calcule la hauteur d'une caméra pour un écran en splitscreen
    /// </summary>
    /// <param name="NumberOfCameras">Nombre total de caméras</param>
    /// <returns>Hauteur de la caméra</returns>
    public static float GetSplitHeight(int NumberOfCameras)
    {
        return (float) (MAX_HEIGHT / Math.Floor( Math.Sqrt(GetNextCeilingNumber(NumberOfCameras)) ));
    }

    /// <summary>
    /// Calcule la largeur d'une caméra pour un écran en splitscreen
    /// </summary>
    /// <param name="NumberOfCameras">Nombre total de caméras</param>
    /// <returns>Largeur de la caméra</returns>
    public static float GetSplitWidth(int NumberOfCameras)
    {
        return (float) (MAX_WIDTH / Math.Ceiling( Math.Sqrt(GetNextCeilingNumber(NumberOfCameras)) ));
    }

    /// <summary>
    /// Calcule le ViewportRect d'une caméra dans un ensemble de caméras en splitscreen
    /// </summary>
    /// <param name="NumberOfCameras">Nombre total de caméras</param>
    /// <param name="CameraIndex">Position de la caméra, en comptant de droite à gauche et de haut en bas</param>
    /// <returns>ViewportRect à utiliser directement</returns>
    public static Rect GetViewportRect(int NumberOfCameras, int CameraIndex)
    {
        float rectX = 0;
        float rectY = 0;

        for (int i = 0; i < CameraIndex; i++)
        {
            rectX += GetSplitWidth(NumberOfCameras);
            if (rectX >= MAX_WIDTH)
            {
                rectX = 0;
                rectY += GetSplitHeight(NumberOfCameras);
            }
        }
        
        float rectWidth = GetSplitWidth(NumberOfCameras);
        float rectHeight = GetSplitHeight(NumberOfCameras);
        return new Rect(rectX, rectY, rectWidth, rectHeight);
    }

    /// <summary>
    /// Calcule le prochain nombre approprié pour le calcul de la division de l'écran par racine
    /// </summary>
    /// <param name="NumberOfCameras">Nombre total de caméra</param>
    /// <returns>Prochain nombre entier adapté au calcul pour le splitscreen</returns>
    private static int GetNextCeilingNumber(int NumberOfCameras)
    {
        double square = Math.Sqrt(NumberOfCameras);
        while (square - Math.Truncate(square) > 0.5f)
        {
            NumberOfCameras++;
            square = Math.Sqrt(NumberOfCameras);
        }
        return NumberOfCameras;
    }
}
