using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class PlayerManager : MonoBehaviour
{

    private static PlayerManager m_Instance;

    public static PlayerManager Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<PlayerManager>();
            }

            return m_Instance;
        }
    }

    public static string PlayersDataPath {get; private set;}

    private List<Player> m_PlayersList = new List<Player>();

    private uint m_NextPlayerId = 1;

    private void Awake()
    {
        PlayersDataPath = Application.dataPath + "/players";
        LoadPlayersFromFiles();
    }

    private void LoadPlayersFromFiles()
    {
        DirectoryInfo info = new DirectoryInfo(PlayerManager.PlayersDataPath);
        FileInfo[] playerFiles = info.GetFiles("*.json");
        foreach (FileInfo file in playerFiles)
        {
            Player player = CreatePlayerFromFile(file);
            m_PlayersList.Add(player);
            if (player.Id >= m_NextPlayerId) m_NextPlayerId = (player.Id + 1);
        }
    }

    public void SavePlayer(Player player)
    {
        string serializedPlayer = player.ToString();
        string path = PlayersDataPath + "/" + player.Id + ".json";
        if (!Directory.Exists(PlayersDataPath))
        {
            Directory.CreateDirectory(PlayersDataPath);
        }
        if (File.Exists(path))
        {
            File.Delete(path);
        }
        File.WriteAllText(path, serializedPlayer);
    }

    public Player CreatePlayer(string name, SerializableDictionary<string, KeyCode> keybinds, int nbShoot, int nbDeath, int nbKills, int nbWins)
    {
        return new Player(m_NextPlayerId, name, keybinds, nbShoot, nbDeath, nbKills, nbWins);
    }

    public Player CreatePlayerFromFile(FileInfo file)
    {
        string json = FileSystemTools.ReadFromFile(file);
        return CreatePlayerFromJson(json);
    }

    public Player CreatePlayerFromJson(string json)
    {
        return JsonUtility.FromJson<Player>(json);
    }

    public List<Player> GetPlayersFromFiles()
    {
        if (m_PlayersList.Count == 0)
        {
            LoadPlayersFromFiles();
        }

        return m_PlayersList;
    }
}
