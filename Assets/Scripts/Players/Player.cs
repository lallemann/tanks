using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Player
{
    public uint Id;
    
    public string Name;

    public SerializableDictionary<string, KeyCode> Keybinds;

    public int nbShoot;
    
    public int nbDeath;

    public int nbKills;

    public int nbWins;

    public Player(uint id, string name, SerializableDictionary<string, KeyCode> keybinds, int nbShoot, int nbDeath, int nbKills, int nbWins)
    {
        this.Id = id;
        this.Name = name;
        this.Keybinds = keybinds;
        this.nbShoot = nbShoot;
        this.nbDeath = nbDeath;
        this.nbKills = nbKills;
        this.nbWins = nbWins;
    }

    public override string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}
