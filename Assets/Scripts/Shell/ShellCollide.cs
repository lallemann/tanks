using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellCollide : MonoBehaviour
{
    public float m_DamageDealtToTanks = 40f;

    public float m_ExplosionPower = 100f;

    public float m_ExplosionRadius = 2f;

    public float m_ExplosionUpwardsBlow = 50f;

    public float m_MaxLifeTime = 1f;

    public int m_Rebounds = 3;

    public Player Player;

    void OnCollisionEnter(Collision collisionInfo)
    {
        // Si la balle touche un tank
        if (collisionInfo.collider.GetComponent<TankHealth>())
        {
            collisionInfo.collider.GetComponent<TankHealth>().TakeDamage(m_DamageDealtToTanks, this.Player);
            BlowCollider(collisionInfo.collider);
            InstantDestroy();
        }
        // Si la balle peut rebondir
        else if (CheckForRebound(collisionInfo))
        {
            DecreaseRebounds();
        }

        // Si la balle a effectué tous ses rebonds
        if (m_Rebounds == 0)
        {
            InstantDestroy();
        }              
    }

    void OnCollisionStay(Collision collisionInfo)
    {
        InstantDestroy();
    }

    private void BlowCollider(Collider collider)
    {
        // collider.attachedRigidbody.AddExplosionForce(m_ExplosionPower * 1000, transform.position, m_ExplosionRadius, m_ExplosionUpwardsBlow, ForceMode.Impulse);
        Rigidbody rb = GetComponent<Rigidbody>();        
        rb.AddForce(-collider.transform.forward * (m_ExplosionPower * 5000), ForceMode.Impulse);        
        rb.AddForce(collider.transform.up * (m_ExplosionPower * 2500), ForceMode.Impulse);
    }

    bool CheckForRebound(Collision collisionInfo)
    {
        bool rebound = true;
        // Si la balle touche un tank
        if (collisionInfo.collider.GetComponent<TankCollide>())
        {
            rebound = false;
        }
        
        return rebound;
    }

    void DecreaseRebounds()
    {
        m_Rebounds --;
    }

    private void InstantDestroy()
    {
        Destroy(gameObject, 0);
    }
}
