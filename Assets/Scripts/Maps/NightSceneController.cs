using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NightSceneController : MonoBehaviour, IGameController
{
    public GameObject[] m_SpawnPlatformsList;

    public float m_SpawnHeightAbovePlateform = 5f;

    public GameObject m_PlayerTankPrefab;

    public GameObject m_PlayerCameraPrefab;

    public List<Player> m_CurrentPlayersList;

    public List<GameObject> m_CurrentTanksList;

    private int m_NbDeadTanks = 0;

    private float m_CurrentTimer = 0f;
    private float m_CurrentTimerSeconds = 0f;
    private bool m_IsTimerActive = false;

    public float TimeLimit = 10;

    /// <summary>
    /// Associates a tank with a spawning platform.
    /// Dictionary<tank: GameObject, spawnPlatform: GameObject>
    /// </summary>
    private Dictionary<GameObject, GameObject> m_TanksSpawnPlatforms;

    // Start is called before the first frame update
    void Start()
    {
        m_SpawnPlatformsList = GameObject.FindGameObjectsWithTag("SpawnPlateform");
        GameInitialization();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTimer();
        if (IsTimeLimitReached()) 
        {
            StopTimer();
            GameOver();            
        }
    }

    /// <summary>
    /// Initialisation de la carte
    /// </summary>
    public void GameInitialization()
    {
        LoadFromGameManager();
        CreateTanksFromPlayers();
        
        m_TanksSpawnPlatforms = AssociateTanksToSpawnPlatforms(m_CurrentTanksList, m_SpawnPlatformsList);
        foreach (KeyValuePair<GameObject, GameObject> entry in m_TanksSpawnPlatforms)
        {
            GameObject playerTank = entry.Key;
            GameObject playerSpawnPlatform = entry.Value;
            playerTank.transform.position = 
                playerSpawnPlatform.transform.position + new Vector3(0f, m_SpawnHeightAbovePlateform, 0f);
        }

        TimeLimit = GameManager.Instance.GameDuration * 60;
        StartTimer();
        GameManager.Instance.CurrentStatus = GameManager.GameStatus.Running;
    }
    
    /// <summary>
    /// Pour chaque joueur dans la partie, crée et initialise un tank jouable.
    /// </summary>
    private void CreateTanksFromPlayers()
    {
        int index = 0;
        foreach (Player player in m_CurrentPlayersList)
        {
            GameObject tank = Instantiate(m_PlayerTankPrefab);
            tank = AddPlayerToTank(tank, player);
            AddCameraForTank(tank, index);
            UpdateTankWithRules(tank);
            m_CurrentTanksList.Add(tank);
            index++;
        }
    }

    /// <summary>
    /// Associe le joueur en paramètre au tank en paramètre et initialise ses éléments d'UI en fonction.
    /// </summary>
    /// <param name="tank">Tank à mettre à jour</param>
    /// <param name="player">Joueur à ajouter au tank</param>
    /// <returns>Le tank mis à jour.</returns>
    private GameObject AddPlayerToTank(GameObject tank, Player player)
    {
        // Liaison du joueur associé
        tank.GetComponentInChildren<TankPlayer>().SetPlayer(player);

        // Liaison des contrôles du joueur
        tank.GetComponentInChildren<TankMovement>().UpdateControls(player.Keybinds);
        tank.GetComponentInChildren<TankShooting>().UpdateControls(player.Keybinds);
        tank.GetComponentInChildren<TankLandmine>().UpdateControls(player.Keybinds);

        return tank;
    }

    private GameObject UpdateTankWithRules(GameObject tank)
    {
        tank.GetComponent<TankHealth>().m_StartingHealth = GameManager.Instance.TankHealth;
        tank.GetComponent<TankMovement>().m_Speed = GameManager.Instance.TankSpeed;
        tank.GetComponent<TankShooting>().m_BulletDamage = GameManager.Instance.BulletDmg;
        tank.GetComponent<TankShooting>().m_BulletRebounds = GameManager.Instance.BulletRebound;
        tank.GetComponent<TankShooting>().m_ReloadTime = GameManager.Instance.BulletReload;
        tank.GetComponent<TankShooting>().m_RecoilPower = GameManager.Instance.BulletRecoilPower;
        tank.GetComponent<TankLandmine>().m_LandmineDamage = GameManager.Instance.LandmineDmg;
        tank.GetComponent<TankLandmine>().m_LootRadius = GameManager.Instance.LandmineLootRadius;
        tank.GetComponent<TankLandmine>().m_LandmineBaseNb = GameManager.Instance.LandmineBaseNb;
        tank.GetComponent<TankLandmine>().m_MaxCapacity = GameManager.Instance.LandmineMaxCapacity;
        tank.GetComponent<TankRespawn>().LivesLeft = GameManager.Instance.GameRespawnNb;
        return tank;
    }

    /// <summary>
    /// Associe un maximum des tanks en paramètre aux plateformes de spawn de la carte.
    /// </summary>
    /// <param name="tanks">Liste des tanks à associer</param>
    /// <param name="spawnPlatforms">Liste des plateformes à associer</param>
    /// <returns>Dictionnaire<tank, Plateforme></returns>
    private Dictionary<GameObject, GameObject> AssociateTanksToSpawnPlatforms(List<GameObject> tanks, GameObject[] spawnPlatforms)
    {
        Dictionary<GameObject, GameObject> association = new Dictionary<GameObject, GameObject>();

        int platformIndex = 0;
        foreach (GameObject tank in tanks)
        {
            if (platformIndex >= spawnPlatforms.Length)
            {
                platformIndex = 0;                
            }
            association.Add(tank, spawnPlatforms[platformIndex]);
            platformIndex++;
        }
        return association;
    }

    /// <summary>
    /// Crée une caméra qui a pour cible le tank en paramètre.
    /// </summary>
    /// <param name="tank"></param>
    /// <returns>Caméra nouvellement créée.</returns>
    private GameObject AddCameraForTank(GameObject tank, int splitscreenIndex)
    {
        if (m_PlayerCameraPrefab == null)
        {
            m_PlayerCameraPrefab = Resources.Load("Prefabs/PlayerCamera") as GameObject;
        }
        GameObject cam = Instantiate(m_PlayerCameraPrefab);
        cam.GetComponent<FollowPlayer>().SetTarget(tank.transform);
        cam.GetComponent<Camera>().rect = Splitscreen.GetViewportRect(m_CurrentPlayersList.Count, splitscreenIndex);
        cam.name = tank.GetComponent<TankPlayer>().m_player.Id.ToString();
        cam.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
        cam.GetComponent<Camera>().backgroundColor = new Color32(1, 12, 29, 0);

        return cam;
    }

    /// <summary>
    /// Charge la liste des joueurs depuis les joueurs actifs du GameManager.
    /// </summary>
    private void LoadFromGameManager()
    {
        m_CurrentPlayersList = GetPlayersFromManager();
    }

    /// <summary>
    /// Retourne la liste des joueurs actifs du GameManager.
    /// </summary>
    /// <returns></returns>
    private List<Player> GetPlayersFromManager()
    {
        return GameManager.Instance.GetPlayers();
    }

    /// <summary>
    /// Retourne true si tous les joueurs sont morts sauf un
    /// </summary>
    /// <returns>true si tous les joueurs sont morts sauf un, false sinon</returns>
    public bool IsGameOver()
    {
        return m_NbDeadTanks >= m_CurrentTanksList.Count - 1;
    }

    /// <summary>
    /// Appelé par un tank lors de sa mort.
    /// </summary>
    /// <param name="deadTank">Tank mort</param>
    public void OnTankDead(GameObject deadTank)
    {
        deadTank.GetComponent<TankPlayer>().m_player.nbDeath++;

        TankRespawn tankRespawnInfo = deadTank.GetComponent<TankRespawn>();

        if (tankRespawnInfo.LivesLeft > 0)
        {
            tankRespawnInfo.LivesLeft--;
            RespawnTank(deadTank);
        }
        else
        {
            m_NbDeadTanks ++;
            GameObject.Destroy(deadTank, 0);
        }

        if(IsGameOver())
        {
            GameOver();
        }
    }

    /// <summary>
    /// Relet à défaut certains paramètres du tank
    /// et le fait réapparaitre sur sa platforme
    /// </summary>
    /// <param name="tank"></param>
    private void RespawnTank(GameObject tank)
    {
        tank.GetComponent<TankHealth>().m_CurrentHealth = GameManager.Instance.TankHealth;
        GameObject spawnPlateform;
        bool hasPlateform = m_TanksSpawnPlatforms.TryGetValue(tank, out spawnPlateform);
        if (hasPlateform)
        {
            tank.transform.position = spawnPlateform.transform.position + new Vector3(0f, m_SpawnHeightAbovePlateform, 0f);
            tank.transform.rotation = spawnPlateform.transform.rotation;
        }    
    }

    /// <summary>
    /// Appelé à la fin du jeu. 
    /// Appelle l'écran de fin de jeu après avoir fait
    /// les traitements adéquats.
    /// </summary>
    public void GameOver()
    {
        foreach(Player p in m_CurrentPlayersList)
        {
            PlayerManager.Instance.SavePlayer(p);
        }

        if (IsGameOver())
        {
            GameObject winner = m_CurrentTanksList.Find(tank => IsTankStillAlive(tank));
            TankPlayer playerWinner = winner.GetComponent<TankPlayer>();
            if (playerWinner != null)
            {
                GameManager.Instance.CurrentStatus = GameManager.GameStatus.Win;
                Winner(playerWinner.m_player);
            }
            else
            {
                GameManager.Instance.CurrentStatus = GameManager.GameStatus.Tie;
            }
        }
        else
        {
            GameManager.Instance.CurrentStatus = GameManager.GameStatus.TimeTie;
        }

        UpdateFinalistTanks();
        LoadGameOverScene();
    }

    private void Winner(Player player)
    {
        player.nbWins++;
    }

    /// <summary>
    /// Filtre la liste des joueurs actuels
    /// pour ne garder que les joueurs vivant
    /// pour l'utiliser dans la scène de fin
    /// </summary>
    private void UpdateFinalistTanks()
    {
        GameManager.Instance.FinalistTanks.Clear();
        foreach (GameObject tank in m_CurrentTanksList)
        {
            if (IsTankStillAlive(tank))
            {
                Object.DontDestroyOnLoad(tank);
                GameManager.Instance.FinalistTanks.Add(tank);
            }
        }
    }

    private void LoadGameOverScene()
    {
        SceneManager.LoadScene("Scenes/Menus/GameOverMenu"); 
    }

    private void StartTimer()
    {
        m_IsTimerActive = true;
    }

    private void UpdateTimer()
    {
        m_CurrentTimer += Time.deltaTime;
        m_CurrentTimerSeconds = m_CurrentTimer % 60;
    }

    private void ResetTimer()
    {
        m_CurrentTimer = 0f;
        m_CurrentTimerSeconds = 0f;
    }

    private void StopTimer()
    {
        m_IsTimerActive = false;
    }

    private bool IsTimeLimitReached()
    {
        return (m_IsTimerActive && m_CurrentTimerSeconds >= TimeLimit);
    }

    private bool IsTankStillAlive(GameObject tank)
    {
        return tank.gameObject != null && tank.GetComponent<TankHealth>().m_Dead == false;
    }
}
