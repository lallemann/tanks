using UnityEngine;

public interface IGameController
{
    public void GameInitialization();

    public bool IsGameOver();

    public void GameOver();

    public void OnTankDead(GameObject deadTank);
}