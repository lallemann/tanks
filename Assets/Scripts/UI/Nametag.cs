using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Nametag : MonoBehaviour
{

    public uint playerId = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Lors du rendu, oriente vers la caméra faisant le rendu
    /// </summary>
    void OnWillRenderObject()
    {
        // transform.localScale = new Vector3(0, 0, 0);
        // if (Camera.current.transform.name.Equals((GetComponentInParent(typeof(TankPlayer)) as TankPlayer).m_player.Id.ToString()))
        // {
        //     transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        //     transform.rotation = Camera.current.transform.rotation;
        // }
        transform.rotation = Camera.current.transform.rotation;
    }
}
