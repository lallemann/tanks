using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextHover : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<TextMesh>().color = new Color(0.88f, 0.9f, 0.9f, 1f);
    }

    void OnMouseEnter() {
        GetComponent<TextMesh>().color = Color.white;
    }

    void OnMouseExit() {
        GetComponent<TextMesh>().color = new Color(0.88f, 0.9f, 0.9f, 1f);
    }
}
