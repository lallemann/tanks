using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandmineBar : MonoBehaviour
{
    public GameObject m_Icon;
    public float m_basePosX = 40;
    public float m_basePosY = 40;    
    public float m_offset = 40;

    private float m_nextPosX;
    private int m_LandmineCount = 0;
    private List<GameObject> m_IconList = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void AddLandmine()
    {
        GameObject icon = Instantiate(m_Icon, transform.position, transform.rotation) as GameObject;
        icon.transform.SetParent(this.transform);
        RectTransform iconRectTransform = icon.GetComponent<RectTransform>();

        iconRectTransform.anchoredPosition = new Vector2(m_basePosX * m_LandmineCount, m_basePosY);
        iconRectTransform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

        m_IconList.Add(icon);
        m_LandmineCount++;
    }

    public void RemoveLandmine()
    {
        GameObject.Destroy(m_IconList[m_IconList.Count - 1]);
        m_IconList.RemoveAt(m_IconList.Count - 1);
        m_LandmineCount--;
    }

    public int GetLandmineCount()
    {
        return m_LandmineCount;
    }
}
