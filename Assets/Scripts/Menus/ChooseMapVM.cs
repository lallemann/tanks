using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ChooseMapVM : MonoBehaviour
{
    public GameObject m_CardsMenuArea;

    public GameObject m_CardPrefab;
    
    public void MapCard_OnClick(string name)
    {
        ClearScreen();
        SceneManager.LoadScene("Scenes/Maps/" + name);
    }

    public void GoRulesButton_OnClick()
    {
        SceneManager.LoadScene("Scenes/Menus/RulesMenu");
    }
    
    private void ClearScreen()
    {
        GameObject canvas = GameObject.Find("Canvas");
        canvas.SetActive(false);
    }

    public void GoBackButton_OnClick()
    {        
        SceneManager.LoadScene("Scenes/Menus/ChoosePlayer");
    }
}
