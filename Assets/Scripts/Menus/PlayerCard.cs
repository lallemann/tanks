using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCard : MonoBehaviour
{
    public Player AssociatedPlayer {get;set;}

    public bool IsReady { get; private set; }

    private ColorBlock ReadyColors;

    private ColorBlock NotReadyColors;

    private ColorBlock m_DefaultButtonColors;

    void Start()
    {
        IsReady = false;
        m_DefaultButtonColors = GetComponent<Button>().colors;
        InitializeColors();
    }

    /// <summary>
    /// Change l'état "prêt" du bouton en fonction de son état actuel
    /// </summary> 
    public void ToggleReady()
    {
        if (IsReady)
        {
            SetNotReady();
        }
        else
        {
            SetReady();
        }
    }

    /// <summary>
    /// Change l'état de la carte à "prêt" et modifie sa couleur
    /// </summary>
    public void SetReady()
    {
        IsReady = true;
        GetComponent<Button>().colors = ReadyColors;
    }

    /// <summary>
    /// Change l'état de la carte à "pas prêt" et modifie sa couleur
    /// </summary>
    public void SetNotReady()
    {
        IsReady = false;
        GetComponent<Button>().colors = NotReadyColors;
    }

    private void InitializeColors()
    {
        ReadyColors = m_DefaultButtonColors;
        ReadyColors.normalColor = new Color32(156, 204, 101, 255);
        ReadyColors.highlightedColor = new Color32(158, 214, 94, 255);
        ReadyColors.pressedColor = new Color32(132, 176, 83, 255);
        ReadyColors.selectedColor = new Color32(152, 207, 89, 255);

        NotReadyColors = m_DefaultButtonColors;
    }

    public void EnableButton()
    {
        GetComponent<Button>().interactable = true;
    }

    public void DisableButton()
    {
        GetComponent<Button>().interactable = false;
    }
}
