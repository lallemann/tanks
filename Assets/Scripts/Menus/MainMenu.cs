using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public bool isStart;
    public bool isQuit;

    void Start()
    {
        GameManager.Instance.Initialize();
    }

    void OnMouseUp()
    {
        if(isStart)
        {
            LoadPlayerChoiceScene();
        }
        if (isQuit)
        {
            Application.Quit();
        }
    } 

    void LoadPlayerChoiceScene()
    {
        SceneManager.LoadScene("Scenes/Menus/ChoosePlayer"); 
    }
}
