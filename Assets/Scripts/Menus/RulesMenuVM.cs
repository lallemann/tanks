using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class RulesMenuVM : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        InitializeInputs();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GoBackButton_OnClick()
    {
        LoadChooseMapScene();
    }

    private void LoadChooseMapScene()
    {
        SceneManager.LoadScene("Scenes/Menus/ChooseMap");
    }
    
    public void TankHealthInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.TankHealth = res;
        }
    }

    public void TankSpeedInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.TankSpeed = res;
        }
    }
    
    public void BulletDmgInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.BulletDmg = res;
        }
    }

    public void BulletRecoilPowerInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.BulletRecoilPower = res;
        }
    }

    public void BulletExplosionPower_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.BulletExplosionPower = res;
        }
    }

    public void BulletExplosionBlow_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.BulletExplosionBlow = res;
        }
    }

    public void BulletReboundInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.BulletRebound = res;
        }
    }

    public void BulletReloadInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.BulletReload = res;
        }
    }

    public void LandmineBaseNbInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.LandmineBaseNb = res;
        }
    }

    public void LandmineMaxCapacity_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.LandmineMaxCapacity = res;
        }
    }


    public void LandmineRespawnInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.LandmineRespawn = res;
        }
    }

    public void LandmineDmgInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.LandmineDmg = res;
        }
    }

    public void LandmineLootRadiusInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.LandmineLootRadius = res;
        }
    }

    public void GameDurationInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.GameDuration = res;
        }
    }

    public void GameBotNbInput_TextChanged(string value)
    {        
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.GameBotNb = res;
        }
    }

    public void GameRespawnNbInput_TextChanged(string value)
    {                
        bool parsed = int.TryParse(value, out int res);
        if (parsed)
        {
            GameManager.Instance.GameRespawnNb = res;
        }
    }

    private void InitializeInputs()
    {
        InputField[] inputFields = GameObject.FindObjectsOfType<InputField>();
        foreach (InputField input in inputFields)
        {
            input.text = input.placeholder.GetComponent<Text>().text;
        }
    }
}
