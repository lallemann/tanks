using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    public TextMesh Title = null;
    public bool isRestart;
    public bool isQuit;

    public Transform tanksSpawnPoint = null;

    public GameObject CrownPrefab = null;

    private List<GameObject> m_spawnedObjects = new List<GameObject>();

    void Start()
    {
        LoadScenario();
    }

    void OnMouseUp()
    {
        if(isRestart)
        {
            Reinitialize();
            ClearScene();
            LoadMainMenu();
        }
        if (isQuit)
        {
            Application.Quit();
        }
    } 

    private void LoadScenario()
    {
        switch (GameManager.Instance.CurrentStatus)
        {
            case GameManager.GameStatus.Win:
                LoadWinScenario();
                break;
            case GameManager.GameStatus.Tie:
                LoadTieScenario();
                break;
            case GameManager.GameStatus.TimeTie:
                LoadTimeTieScenario();
                break;
            default:
                LoadTieScenario();
                break;
        }
    }

    private void LoadWinScenario()
    {
        if (Title != null) Title.text = "Victoire !";
        SpawnWinners();
        for (int i = 0; i < 15; i++)
        {
            SpawnCrown();
        }            
    }

    private void LoadTieScenario()
    {
        if (Title != null) Title.text = "Egalité !";
        SpawnWinners();
    }

    private void LoadTimeTieScenario()
    {
        if (Title != null) Title.text = "Temps écoulé !";
        SpawnWinners();
    }

    private void Reinitialize()
    {
        GameManager.Instance.Initialize();
    }

    private void SpawnWinners()
    {
        foreach (GameObject tank in GameManager.Instance.FinalistTanks)
        {
            try
            {
                tank.transform.Find("TankRenderers/Healthbar").gameObject.SetActive(false);
                tank.transform.Find("TankRenderers/Landminebar").gameObject.SetActive(false);
                tank.transform.Find("TankRenderers/Nametag").gameObject.SetActive(true);
            }          
            catch(System.NullReferenceException e)
            {
                Debug.Log(e.Message);
            }
            if (tanksSpawnPoint != null) tank.transform.position = tanksSpawnPoint.position;
            if (tanksSpawnPoint != null) tank.transform.rotation = tanksSpawnPoint.rotation;
            tank.GetComponent<TankMovement>().Reset();
            tank.GetComponent<TankHealth>().enabled = false;
            tank.GetComponent<TankLandmine>().enabled = false;
            tank.GetComponent<Rigidbody>().velocity = Vector3.zero;
            tank.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            m_spawnedObjects.Add(tank);
        }
    }

    private void SpawnCrown()
    {
        if (CrownPrefab != null && tanksSpawnPoint != null)
        {
            GameObject crown = Instantiate(CrownPrefab,
                                    tanksSpawnPoint.transform.position + Vector3.Scale(CrownPrefab.GetComponent<BoxCollider>().center, CrownPrefab.transform.localScale),
                                    Quaternion.identity) as GameObject;
            m_spawnedObjects.Add(crown);
        }
    }

    private void ClearScene()
    {
        foreach (GameObject item in m_spawnedObjects)
        {
            GameObject.Destroy(item);
        }
    }

    void LoadMainMenu()
    {
        SceneManager.LoadScene("Scenes/Menus/MainMenu"); 
    }

    public void onClickBackButton(){
        SceneManager.LoadScene("Scenes/Menus/MainMenu");
    }
}
