using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ChoosePlayerVM : MonoBehaviour
{

    private List<Player> m_playersList = new List<Player>();

    public GameObject m_CardsMenuArea;

    public GameObject m_CardPrefab;

    public Slider m_Slider;

    public int PlayerNumber { get; private set; }

    private List<PlayerCard> m_PlayerCardsList = new List<PlayerCard>();

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.ClearPlayers();
        PlayerNumber = 1;
        LoadPrefabs();

        m_playersList = PlayerManager.Instance.GetPlayersFromFiles();      
        foreach (Player player in m_playersList)
        {
            AddCard(player);
        }

        GameObject slider = GameObject.Find("Slider") as GameObject;
        slider.GetComponent<Slider>().minValue = 1;
        slider.GetComponent<Slider>().maxValue = m_playersList.Count;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddCard(Player player)
    {
        GameObject card = Instantiate(m_CardPrefab.gameObject) as GameObject;
        card.GetComponentInChildren<Text>().text = player.Name;
        card.transform.SetParent(m_CardsMenuArea.transform);
        card.GetComponent<PlayerCard>().AssociatedPlayer = player;
        Button cardBtn = card.GetComponent<Button>();
        cardBtn.onClick.AddListener(() => PlayerCard_OnClick(cardBtn));            

        m_PlayerCardsList.Add(card.GetComponent<PlayerCard>());

        Button statsBtn = card.transform.Find("StatsButton").GetComponent<Button>();
        statsBtn.onClick.AddListener(() => PlayerStatButton_OnClick(player)); 

        UpdateCardViewerWidth();
    }

    public void PlayerCard_OnClick(Button sender)
    {
        // Initialisation
        PlayerCard senderPlayerCard = sender.GetComponentInChildren<PlayerCard>();

        // Si le clic a pour but de mettre la carte en "prêt"
        // mais que le nombre de joueurs max est déjà atteint
        // on accepte pas le clic
        if (!(!senderPlayerCard.IsReady && IsEveryoneReady()))
        {
            senderPlayerCard.ToggleReady();
        }

        if (IsEveryoneReady())
        {
            AddPlayersToManager();
            GoToMapSelection();
        }
    }

    public void PlayerStatButton_OnClick(Player player)
    {
        string serializedPlayer = player.ToString();
        PlayerPrefs.SetString("PlayerStats", serializedPlayer);
        LoadPlayerStatsScene();
    }

    public void CreatePlayerButton_OnClick()
    {
        LoadCreatePlayerScene();
    }

    private void AddPlayersToManager()
    {
        foreach (PlayerCard card in m_PlayerCardsList)
        {
            if (card.IsReady)
            {
                GameManager.Instance.AddPlayer(card.AssociatedPlayer);
            }
        }
    }

    private void UpdateCardViewerWidth()
    {
        RectTransform rectTransform = m_CardsMenuArea.GetComponent<RectTransform>();
        float spacingBetweenCards = m_CardsMenuArea.GetComponent<HorizontalLayoutGroup>().spacing;
        float totalCardsWidth = m_playersList.Count * m_CardPrefab.GetComponent<RectTransform>().rect.width;
        float totalSpacing = m_playersList.Count * spacingBetweenCards;
        float newTotalWidth = totalSpacing + totalCardsWidth;

        rectTransform.sizeDelta = new Vector2(newTotalWidth, rectTransform.sizeDelta.y);
    }  

    public void NumberPlayerSlider_OnValueChanged()
    {
        PlayerNumber = (int)m_Slider.value;
        // Réinitialisation des cartes
        foreach (PlayerCard card in m_PlayerCardsList)
        {
            card.SetNotReady();
            card.EnableButton();
        }
        // Mise à jour du label
        GameObject.Find("NbPlayersLabel").GetComponent<Text>().text = ("Nombre de joueurs : " + PlayerNumber);
    }

    /// <summary>
    /// Vérifie si tous les joueurs ont séléctionné un profil de joueur
    /// </summary>
    /// <returns>Si tous les joueurs sont prêts</returns>
    private bool IsEveryoneReady()
    {
        int selected = 0;

        foreach (PlayerCard card in m_PlayerCardsList)
        {
            if (card.IsReady)
            {
                selected++;
            }
        }

        if (selected > PlayerNumber)
        {
            Debug.Log("Le nombre de joueurs séléctionnés est supérieur au nombre max.");
        }

        return PlayerNumber != 0 && (selected == PlayerNumber || selected == m_PlayerCardsList.Count);
    }


    /// <summary>
    /// Charge la scène de séléction de mode de jeu
    /// </summary>
    private void GoToMapSelection()
    {
        SceneManager.LoadScene("Scenes/Menus/ChooseMap");
    }

    private void LoadPrefabs()
    {
        if (m_CardPrefab == null)
        {
            m_CardPrefab = Resources.Load("Prefabs/PlayerCard") as GameObject;
        }
    }

    public void GoBackButton_OnClick()
    {
        SceneManager.LoadScene("Scenes/Menus/MainMenu");
    }

    void LoadCreatePlayerScene()
    {
        SceneManager.LoadScene("Scenes/Menus/CreatePlayer"); 
    }  

    void LoadPlayerStatsScene()
    {
        SceneManager.LoadScene("Scenes/Menus/StatsMenu"); 
    }
}
