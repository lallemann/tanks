using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class KeybindManager : MonoBehaviour
{
    private static KeybindManager m_Instance;

    public static KeybindManager Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<KeybindManager>();
            }

            return m_Instance;
        }
    }

    public SerializableDictionary<string, KeyCode> Keybinds { get; private set; }

    private string m_bindName;

    // Start is called before the first frame update
    void Start()
    {
        Keybinds = new SerializableDictionary<string, KeyCode>();  
        ResetKeybinds();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetKeybinds()
    {
        Keybinds.Clear();
        BindKey("FORWARD", KeyCode.Z);
        BindKey("LEFT", KeyCode.Q);
        BindKey("RIGHT", KeyCode.D);
        BindKey("REVERSE", KeyCode.S);
        BindKey("SHOOT", KeyCode.Space);
        BindKey("PLACE_MINE", KeyCode.P);
        BindKey("LOOT_MINE", KeyCode.M);
    }

    public void BindKey(string key, KeyCode code)
    {
        // Si l'action n'a pas encore été assignée à une touche
        if (!Keybinds.ContainsKey(key))
        {
            Keybinds.Add(key, code);
        }
        
        // Si la touche n'a pas encore été assignée a une action
        if (!Keybinds.ContainsValue(code))
        {
            Keybinds[key] = code;
        }
        // Si la touche a déjà été assignée à une action
        else
        {
            IEnumerable<KeyValuePair<string, KeyCode>> duplicates = Keybinds.Where(x => x.Value == code);
            List<KeyValuePair<string, KeyCode>> duplicatesList = new List<KeyValuePair<string, KeyCode>>(duplicates);

            foreach (KeyValuePair<string, KeyCode> duplicatePair in duplicates)
            {
                // Mise à jour du duplicata dans le dictionnaire
                Keybinds[duplicatePair.Key] = KeyCode.None;

                // Mise à jour de la vue
                CreatePlayerVM.Instance.UpdateKeyText(duplicatePair.Key, KeyCode.None);
            }
        }

        Keybinds[key] = code;
        CreatePlayerVM.Instance.UpdateKeyText(key, code);
        m_bindName = string.Empty;
    }
}
