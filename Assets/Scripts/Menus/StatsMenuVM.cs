using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StatsMenuVM : MonoBehaviour
{

    private Player m_player;

    public Text TitleLabel;

    public Text StatShotsLabel;

    public Text StatKillsLabel;

    public Text StatDeathsLabel;

    public Text StatWinsLabel;

    // Start is called before the first frame update
    void Start()
    {
        m_player = JsonUtility.FromJson<Player>(PlayerPrefs.GetString("PlayerStats"));
        InitializeFields();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void InitializeFields()
    {
        TitleLabel.text = "Statistiques de " + m_player.Name;
        StatShotsLabel.text = m_player.nbShoot.ToString();
        StatKillsLabel.text = m_player.nbKills.ToString();
        StatDeathsLabel.text = m_player.nbDeath.ToString();
        StatWinsLabel.text = m_player.nbWins.ToString();
    }

    public void GoBackButton_OnClick()
    {
        SceneManager.LoadScene("Scenes/Menus/ChoosePlayer");
    }

}
