using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class CreatePlayerVM : MonoBehaviour
{

    private static CreatePlayerVM m_Instance;

    public static CreatePlayerVM Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<CreatePlayerVM>();
            }

            return m_Instance;
        }
    }

    private CanvasGroup m_KeybindsMenu;

    private string m_PlayerName = "Joueur";

    private GameObject[] m_KeybindButtons;

    private Button m_KeybindBtnWaitingForInput;

    private KeyCode m_SelectedKeyCode = KeyCode.None;

    private void Awake()
    {
        m_KeybindButtons = GameObject.FindGameObjectsWithTag("Keybind");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void NameInput_TextChanged(string newText)
    {
        m_PlayerName = newText;
    }

    public void KeybindsToggle_OnValueChanged(bool value)
    {
        if (value)
        {
            ShowKeybindsMenu();
        }
        else
        {
            HideKeybindsMenu();
        }
    }

    private void ShowKeybindsMenu()
    {
        m_KeybindsMenu.alpha = 1f;
        m_KeybindsMenu.blocksRaycasts = true;
    }

    private void HideKeybindsMenu()
    {
        m_KeybindsMenu.alpha = 0f;
        m_KeybindsMenu.blocksRaycasts = false;
    }

    public void UpdateKeyText(string key, KeyCode code)
    {
        Text textComponent = Array.Find(m_KeybindButtons, x => x.name == key).GetComponentInChildren<Text>();
        textComponent.text = code.ToString();
    }

    public void KeybindButton_OnClick()
    {
        // Initialisation
        Button sender = EventSystem.current.currentSelectedGameObject.gameObject.GetComponent<Button>();
        Text senderTextComponent = sender.GetComponentInChildren<Text>();

        // Attente d'une entrée utilisateur
        m_KeybindBtnWaitingForInput = sender;
        senderTextComponent.text = "...";
        StartCoroutine(WaitForKeycodeInput());        
    }

    private IEnumerator WaitForKeycodeInput()
    {
        while (!Input.anyKeyDown && m_SelectedKeyCode == KeyCode.None)
        {
            yield return null;
        }

        foreach(KeyCode key in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKey(key))
            {
                m_SelectedKeyCode = key;
                BindKeyAfterInput();
                break;
            }                
        }
    }

    private void BindKeyAfterInput()
    {
        KeyCode selectedKeyCode = m_SelectedKeyCode;
        KeybindManager.Instance.BindKey(m_KeybindBtnWaitingForInput.name, selectedKeyCode);
        UpdateKeyText(m_KeybindBtnWaitingForInput.name, selectedKeyCode);
        m_SelectedKeyCode = KeyCode.None;
    }

    public void ValidateButton_OnClick()
    {
        ConfirmForm();
        LoadChoosePlayerScene();
    }

    private void ConfirmForm()
    {
        Player newPlayer = PlayerManager.Instance.CreatePlayer(m_PlayerName, KeybindManager.Instance.Keybinds,0,0,0,0);
        PlayerManager.Instance.SavePlayer(newPlayer);
        KeybindManager.Instance.ResetKeybinds();
    }

    public void GobackButton_OnClick()
    {
        LoadChoosePlayerScene();
    }

    private void LoadChoosePlayerScene()
    {
        SceneManager.LoadScene("Scenes/Menus/ChoosePlayer"); 
    }    
}