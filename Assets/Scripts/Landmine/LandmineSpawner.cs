using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandmineSpawner : MonoBehaviour
{

    public float m_TimeBetweenSpawns = 5f;
    public GameObject m_LandminePrefab;

    private float m_CurrentTimer = 0f;
    private float m_CurrentTimerSeconds = 0f;
    private bool m_IsTimerActive = false;

    private bool m_IsHaloEnabled = false;
    private Behaviour m_Halo;

    // Start is called before the first frame update
    void Start()
    {
        m_Halo = (Behaviour)GetComponent("Halo");
        InitializeHalo();
    }

    // Update is called once per frame
    void Update()
    {
        // Si le spawner a déjà une mine
        if (IsThereLandmineAbove())
        {            
            if (m_IsTimerActive)
            {
                StopTimer();
            }
            if (!m_IsHaloEnabled)
            {
                EnableHalo();
            }
        }
        // Si le spawner est libre
        else
        {
            if (m_IsTimerActive)
            {
                UpdateTimer();

                if (IsTimeLimitReached())
                {
                    SpawnLandmine();
                    ResetTimer();
                    StopTimer();
                }
            }
            else
            {
                StartTimer();
            }

            if (m_IsHaloEnabled)
            {
                DisableHalo();
            }
        }
    }

    private bool IsThereLandmineAbove()
    {
        return FindLandminesAbove().Count > 0;
    }

    private List<Collider> FindLandminesAbove()
    {
        List<Collider> landminesList = new List<Collider>();
        Collider[] colliders = Physics.OverlapSphere(transform.position + new Vector3(0f, transform.localScale.y, 0f), transform.localScale.x);  
        
        foreach(Collider c in colliders)
        {
            if(c.GetComponent<LandmineCollide>())
            {
                landminesList.Add(c);
            }
        }

        return landminesList; 
    }

    private void StartTimer()
    {
        m_IsTimerActive = true;
    }

    private void UpdateTimer()
    {
        m_CurrentTimer += Time.deltaTime;
        m_CurrentTimerSeconds = m_CurrentTimer % 60;
    }

    private void ResetTimer()
    {
        m_CurrentTimer = 0f;
        m_CurrentTimerSeconds = 0f;
    }

    private void StopTimer()
    {
        m_IsTimerActive = false;
    }

    private bool IsTimeLimitReached()
    {
        return (m_IsTimerActive && m_CurrentTimerSeconds >= m_TimeBetweenSpawns);
    }

    void SpawnLandmine()
    {
        Vector3 position = transform.position;
        Quaternion rotation = transform.rotation;
        rotation.eulerAngles = new Vector3(-90f, 0, 0);
        GameObject mine =
            Instantiate(m_LandminePrefab, position + new Vector3(0f, transform.localScale.y, 0f), rotation) as GameObject;
    }

    private void InitializeHalo()
    {
        if (IsThereLandmineAbove())
        {
            EnableHalo();
        }
        else
        {
            DisableHalo();
        }
    }

    private void EnableHalo()
    {
        m_IsHaloEnabled = true;
        if (m_Halo != null)
        {
            m_Halo.enabled = true;
        }        
        
    }

    private void DisableHalo()
    {
        m_IsHaloEnabled = false;
        if (m_Halo != null)
        {
            m_Halo.enabled = false;
        }    
    }
}
