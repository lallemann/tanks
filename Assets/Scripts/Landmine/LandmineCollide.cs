using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandmineCollide : MonoBehaviour
{
    // Rayon de l'explosion
    public float m_ExplosionRadius = 7.0f;
    
    // Puissance de l'explosion
    public float m_ExplosionPower = 500.0f;

    // Modificateur de l'explosion, souffle vers le haut
    public float m_ExplosionUpwardsBlow = 300.0f; 

    // Dégats donnés aux tanks lors de l'explosion
    public float m_DamageDealtToTanks = 100f;

    // Centre de l'explosion
    private Vector3 m_ExplosionCenter;
    
    /**
     * Première méthode appelée
     * Utilisée pour les initialisations
     */
    private void Start()
    {
        m_ExplosionCenter = transform.position;
    }

    /**
     * Appellée lors d'une collision
     * Déclenche l'explosion 
     */
    void OnCollisionEnter(Collision collisionInfo)
    {
        // Explosion seulement si l'objet est un tank
        if (collisionInfo.collider.GetComponent<TankCollide>())
        {
            Explode();
        }
    }

    /**
     * Explosion de la mine
     * Entraine des calculs de physique et détruit la mine. 
     */
    void Explode()
    {                   
        // Traitement sur tous les objets touchés
        foreach (Collider collider in GetObjectsWithinExplosionRadius())
        {            
            Rigidbody rigidbody = collider.GetComponent<Rigidbody>();
            TankHealth tankHealth = collider.GetComponent<TankHealth>();

            // Effet de l'explosion seulement si l'objet est un tank et s'il est à découvert
            if (collider.gameObject.tag == "Player" && IsTargetTouchedByExplosion(collider))
            {
                if (rigidbody)
                {
                    Debug.Log("m_ExplosionPower: " + m_ExplosionPower);
                    Debug.Log("m_ExplosionCenter: " + m_ExplosionCenter);
                    Debug.Log("m_ExplosionRadius: " + m_ExplosionRadius);
                    Debug.Log("m_ExplosionUpwardsBlow: " + m_ExplosionUpwardsBlow);
                    rigidbody.AddExplosionForce(m_ExplosionPower, m_ExplosionCenter, m_ExplosionRadius, m_ExplosionUpwardsBlow);
                }                    

                if (tankHealth)
                {
                    tankHealth.TakeDamage(CalculateDamage(collider.transform.position));
                }
                    
                InstantDestroy();
            }                
        }
    }

    Collider[] GetObjectsWithinExplosionRadius()
    {
        Collider[] colliders = Physics.OverlapSphere(m_ExplosionCenter, m_ExplosionRadius);  
        return colliders; 
    }

    bool IsTargetTouchedByExplosion(Collider target)
    {
        return (Physics.Linecast(transform.position, target.transform.position));
    }

    private void InstantDestroy()
    {
        Destroy(gameObject, 0);
    }

    public void Remove()
    {
        InstantDestroy();
    }

    private float CalculateDamage(Vector3 targetPosition)
    {
        // Calculate the amount of damage a target should take based on it's position.
        return m_DamageDealtToTanks * ( 1 / Vector3.Distance(transform.position, targetPosition));
    }
}
