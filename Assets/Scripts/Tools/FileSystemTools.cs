using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class FileSystemTools : MonoBehaviour
{
    private static FileSystemTools m_Instance;

    public static FileSystemTools Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType<FileSystemTools>();
            }

            return m_Instance;
        }
    }

    public static string ReadFromFile(FileInfo file)
    {
        StreamReader sr = new StreamReader(file.FullName);
        string fileContent = sr.ReadToEnd();
        sr.Close();
        return fileContent;
    }
}
