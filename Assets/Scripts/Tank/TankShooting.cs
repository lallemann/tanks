﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankShooting : MonoBehaviour
{
    public Rigidbody m_Bullet;
    public float m_bulletSpeed = 15f;
    public int m_BulletDamage = 40;
    public int m_BulletRebounds = 3;
    public Transform m_FireTransform;
    public KeyCode m_keyShoot = KeyCode.Space;
    public int m_MaxSimultaneousBullets = 5;
    public float m_RecoilPower = 300f;

    public float m_ReloadTime = 1f;

    private float m_CurrentTimer = 0f;
    private float m_CurrentTimerSeconds = 0f;

    private bool m_CanFire;    
    private List<Rigidbody> m_BulletList= new List<Rigidbody>();

    private void Update()
    {
        UpdateTimer();

        if (IsTimeLimitReached())
        {
            m_CanFire = true;
            ResetTimer();
        }

        if (m_keyShoot != KeyCode.None && Input.GetKeyDown(m_keyShoot) && m_CanFire)
        {
            Fire();
            m_CanFire = false;
        }

        if (m_BulletList.Count < m_MaxSimultaneousBullets)
        {
            CleanBulletList();
        }        
    }

    private void Fire()
    {
        // Create an instance of the shell and store a reference to it's rigidbody.
        Rigidbody bullet =
            Instantiate(m_Bullet, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;

        bullet.GetComponent<ShellCollide>().Player = GetComponent<TankPlayer>().m_player;
        bullet.GetComponent<ShellCollide>().m_DamageDealtToTanks = this.m_BulletDamage;
        bullet.GetComponent<ShellCollide>().m_Rebounds = this.m_BulletRebounds;
        bullet.GetComponent<ShellCollide>().m_ExplosionPower = GameManager.Instance.BulletExplosionPower;
        bullet.GetComponent<ShellCollide>().m_ExplosionUpwardsBlow = GameManager.Instance.BulletExplosionBlow;
        bullet.velocity = m_FireTransform.forward * m_bulletSpeed;
        m_BulletList.Add(bullet);        
        GetComponent<TankPlayer>().m_player.nbShoot++;

        Recoil();
    }

    private void CleanBulletList()
    {
        for (int i = 0; i < m_BulletList.Count; i++)
        {
            if (m_BulletList[i] == null)
            {
                m_BulletList.RemoveAt(i);
                i--;
            }
        }
    }

    private void Recoil()
    {
        Rigidbody rb = GetComponent<Rigidbody>();        
        rb.AddRelativeForce(-Vector3.forward * m_RecoilPower, ForceMode.Impulse);        
        rb.AddRelativeForce(Vector3.up * (m_RecoilPower/4), ForceMode.Impulse);
    }

    public void UpdateControls(SerializableDictionary<string, KeyCode> controlsDict)
    {
        KeyCode shootKeycode;
        if (controlsDict.TryGetValue("SHOOT", out shootKeycode))
        {
            m_keyShoot = shootKeycode;
        }
    }

    private void UpdateTimer()
    {
        m_CurrentTimer += Time.deltaTime;
        m_CurrentTimerSeconds = m_CurrentTimer % 60;
    }

    private void ResetTimer()
    {
        m_CurrentTimer = 0f;
        m_CurrentTimerSeconds = 0f;
    }

    private bool IsTimeLimitReached()
    {
        return (m_CurrentTimerSeconds >= m_ReloadTime);
    }
}