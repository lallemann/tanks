using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMovement2 : MonoBehaviour {

	public KeyCode m_ForwardKeyCode = KeyCode.Z;
    public KeyCode m_ReverseKeyCode = KeyCode.S;
    public KeyCode m_LeftKeyCode = KeyCode.Q;
    public KeyCode m_RightKeyCode = KeyCode.D;

    private float MoveInput = 0;
    private float TurnInput = 0;

    // rotation that occurs in angles per second holding down input
    public float rotationRate = 360;

    // units moved per second holding down move input
    public float moveRate = 10;

    private Rigidbody rb;

    private void Start() 
    {
        rb = GetComponent<Rigidbody>();
    }

	// Update is called once per frame
	private void Update () 
    {
        MoveInput = (Input.GetKeyDown(m_ForwardKeyCode)) ? 1 : 0;
        MoveInput = (Input.GetKeyDown(m_ReverseKeyCode)) ? -1 : 0;
        
        TurnInput = (Input.GetKeyDown(m_RightKeyCode)) ? 1 : 0;
        TurnInput = (Input.GetKeyDown(m_LeftKeyCode)) ? -1 : 0;

        ApplyInput(MoveInput, TurnInput);
	}

    private void ApplyInput(float moveInput, float turnInput) 
    {
		Move(moveInput);
		Turn(turnInput);
    }

    private void Move(float input) 
    {
        rb.AddForce(transform.forward * input * moveRate, ForceMode.Force);
    }

    private void Turn(float input)
    {
        transform.Rotate(0, input * rotationRate * Time.deltaTime, 0);
    }

    public void UpdateControls(SerializableDictionary<string, KeyCode> controlsDict)
    {
        KeyCode forwardKeycode;
        if (controlsDict.TryGetValue("FORWARD", out forwardKeycode))
        {
            m_ForwardKeyCode = forwardKeycode;
        }        

        KeyCode leftKeycode;
        if (controlsDict.TryGetValue("LEFT", out leftKeycode))
        {
            m_LeftKeyCode = leftKeycode;
        } 

        KeyCode rightKeycode;
        if (controlsDict.TryGetValue("RIGHT", out rightKeycode))
        {
            m_RightKeyCode = rightKeycode;
        } 

        KeyCode reverseKeycode;
        if (controlsDict.TryGetValue("REVERSE", out reverseKeycode))
        {
            m_ReverseKeyCode = reverseKeycode;
        } 
    }
}