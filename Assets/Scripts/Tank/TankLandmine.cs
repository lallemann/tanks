using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankLandmine : MonoBehaviour
{
    public KeyCode m_keyPlace = KeyCode.P;
    public KeyCode m_keyCollect = KeyCode.M;
    public int m_MaxCapacity = 3;
    public float m_LootRadius = 10f;
    public int m_LandmineBaseNb = 0;
    public float m_LandmineDamage = 100f;
    public LandmineBar m_Landminebar;
    public GameObject m_Landmine;

    public GameObject m_landmineBarPrefab;
    public float deployOffset = -3f;
    
    private int m_currentLandmines = 0;

    // Start is called before the first frame update
    void Start()
    {
        InitializeLandmineBar();
        InitializeStartLandmines();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_keyCollect != KeyCode.None && Input.GetKeyDown(m_keyCollect) && m_currentLandmines < m_MaxCapacity) SearchAndCollectLandmines();

        if (m_keyPlace != KeyCode.None && Input.GetKeyDown(m_keyPlace)) DeployLandmine();
    }

    private void InitializeLandmineBar()
    {
        if (m_landmineBarPrefab == null)
        {
            m_landmineBarPrefab = Resources.Load("Prefabs/Landminebar") as GameObject;
        }        
        if (m_Landminebar == null)
        {
            GameObject landmineBar = Instantiate(m_landmineBarPrefab, transform.position, Quaternion.identity) as GameObject;
            landmineBar.gameObject.transform.SetParent(gameObject.transform);
            m_Landminebar = landmineBar.GetComponent<LandmineBar>();
        }        
    }

    private void InitializeStartLandmines()
    {
        for (int i = 0; i < m_LandmineBaseNb; i++)
        {
            CollectLandmine();
        }
    }

    void SearchAndCollectLandmines()
    {
        List<Collider> landminesList =  GetLandminesWithinLootRadius();
        foreach(Collider c in landminesList)
        {
            if (IsLandmineLootable(c.GetComponent<LandminePlayer>()))
            {
                CollectLandmine();
                DestroyLandmineObject(c);
            }            
        }
    }

    private bool IsLandmineLootable(LandminePlayer landmine)
    {
        uint tankPlayerId = gameObject.GetComponent<TankPlayer>().m_player.Id;
        
        try
        {
            uint landminePlayerId = landmine.Player.Id;
        }
        catch (System.NullReferenceException)
        {
            return true;
        }        

        return landmine.Player.Id == 0 || tankPlayerId == landmine.Player.Id;
    }

    List<Collider> GetLandminesWithinLootRadius()
    {
        List<Collider> landminesList = new List<Collider>();
        Collider[] colliders = Physics.OverlapSphere(transform.position + new Vector3(0f, 3f, 0f), m_LootRadius);  
        
        foreach(Collider c in colliders)
        {
            if(c.GetComponent<LandmineCollide>() && IsLandmineLootable(c.gameObject.GetComponent<LandminePlayer>()))
            {
                landminesList.Add(c);
            }
        }

        return landminesList; 
    }

    void DestroyLandmineObject(Collider landmine)
    {
        landmine.GetComponent<LandmineCollide>().Remove();
    }

    void CollectLandmine()
    {
        m_currentLandmines++;
        m_Landminebar.AddLandmine();
    }

    void DeployLandmine()
    {
        if (m_currentLandmines > 0)
        {
            InstantiateLandmine();
            m_currentLandmines--;
            m_Landminebar.RemoveLandmine();
        }        
    }

    void InstantiateLandmine()
    {
        Vector3 position = transform.position + (transform.forward * deployOffset) + (transform.up * 2f);
        Quaternion rotation = transform.rotation;
        rotation.eulerAngles = new Vector3(-90f, 0, 0);
        GameObject mine =
            Instantiate(m_Landmine, position, rotation) as GameObject;
        mine.GetComponent<LandminePlayer>().Player = gameObject.GetComponent<TankPlayer>().m_player;
        mine.GetComponent<LandmineCollide>().m_DamageDealtToTanks = m_LandmineDamage;
    }

    public void UpdateControls(SerializableDictionary<string, KeyCode> controlsDict)
    {
        KeyCode placeKeycode;
        if (controlsDict.TryGetValue("PLACE_MINE", out placeKeycode))
        {
            m_keyPlace = placeKeycode;
        }

        KeyCode collectKeycode;
        if (controlsDict.TryGetValue("LOOT_MINE", out collectKeycode))
        {
            m_keyCollect = collectKeycode;
        }
    }
}
