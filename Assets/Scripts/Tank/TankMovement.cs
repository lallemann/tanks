﻿using UnityEngine;

public class TankMovement : MonoBehaviour
{
    public float m_Speed = 750f;

    // Contrôles utilisateur
    public KeyCode keyMoveForward = KeyCode.Z;
    public KeyCode keyMoveReverse = KeyCode.S;
    public KeyCode keyRotateRight = KeyCode.D;
    public KeyCode keyRotateLeft = KeyCode.Q;

    // Variables de déplacement
    bool moveForward = false;
    bool moveReverse = false;
    float moveSpeed = 1f;
    float moveSpeedReverse = 0f;
    float moveAcceleration = 0.5f;
    bool rotateRight = false;
    bool rotateLeft = false;
    float rotateSpeedRight = 0f;
    float rotateSpeedLeft = 0f;
    float rotateAcceleration = 4f;
    float rotateDeceleration = 10f;
    float rotateSpeedMax = 130f;

    Rigidbody m_Rigidbody;

    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        GetMoveInput();
        GetTurnInput();
    }

    void FixedUpdate()
    {
        Move();
        Turn();
        FixPosition();
    }

    private void Move()
    {
        if (moveForward)
        {
            moveSpeed = (moveSpeed < m_Speed) ? moveSpeed + moveAcceleration : m_Speed;
            m_Rigidbody.AddTorque(transform.right * moveSpeed * 50f * Time.deltaTime);
            m_Rigidbody.MovePosition(m_Rigidbody.position + (transform.forward * moveSpeed * Time.deltaTime));
        }  

        if (moveReverse)
        {
            moveSpeedReverse = (moveSpeedReverse < m_Speed) ? moveSpeedReverse + moveAcceleration : m_Speed;
            m_Rigidbody.AddTorque(transform.right * moveSpeed * -50f * Time.deltaTime);
            m_Rigidbody.MovePosition(m_Rigidbody.position + (transform.forward * moveSpeed * -1 * Time.deltaTime));
        }
    }


    private void Turn()
    {
        if (rotateLeft)
        {
            rotateSpeedLeft = (rotateSpeedLeft < rotateSpeedMax) ? rotateSpeedLeft + rotateAcceleration : rotateSpeedMax;
        }
        else
        {
            rotateSpeedLeft = (rotateSpeedLeft > 0) ? rotateSpeedLeft - rotateDeceleration : 0;
        }
        transform.Rotate(0f, rotateSpeedLeft * Time.deltaTime * -1f, 0f);

        if (rotateRight)
        {
            rotateSpeedRight = (rotateSpeedRight < rotateSpeedMax) ? rotateSpeedRight + rotateAcceleration : rotateSpeedMax;
        }
        else
        {
            rotateSpeedRight = (rotateSpeedRight > 0) ? rotateSpeedRight - rotateDeceleration : 0;
        }
        transform.Rotate(0f, rotateSpeedRight * Time.deltaTime, 0f);
    }

    void FixPosition()
    {
        Vector3 currentRotation = transform.rotation.eulerAngles;
        Vector3 correctRotation = new Vector3(0f, currentRotation.y, 0f);
        if ((currentRotation.z >= 90 || currentRotation.z <= -90) ||
            (currentRotation.x >= 90 || currentRotation.x <= -90))
            {
                transform.rotation = Quaternion.Lerp(Quaternion.Euler(currentRotation), Quaternion.Euler(correctRotation), Time.deltaTime * 10f);
            }
    }

    void GetMoveInput()
    {
        moveForward = (Input.GetKeyDown(keyMoveForward)) ? true : moveForward;
        moveForward = (Input.GetKeyUp(keyMoveForward)) ? false : moveForward;

        moveReverse = (Input.GetKeyDown(keyMoveReverse)) ? true : moveReverse;
        moveReverse = (Input.GetKeyUp(keyMoveReverse)) ? false : moveReverse;
    }

    void GetTurnInput()
    {
        rotateLeft = (Input.GetKeyDown(keyRotateLeft)) ? true : rotateLeft;
        rotateLeft = (Input.GetKeyUp(keyRotateLeft)) ? false : rotateLeft;

        rotateRight = (Input.GetKeyDown(keyRotateRight)) ? true : rotateRight;
        rotateRight = (Input.GetKeyUp(keyRotateRight)) ? false : rotateRight;
    }

    public void UpdateControls(SerializableDictionary<string, KeyCode> controlsDict)
    {
        KeyCode forwardKeycode;
        if (controlsDict.TryGetValue("FORWARD", out forwardKeycode))
        {
            keyMoveForward = forwardKeycode;
        }        

        KeyCode leftKeycode;
        if (controlsDict.TryGetValue("LEFT", out leftKeycode))
        {
            keyRotateLeft = leftKeycode;
        } 

        KeyCode rightKeycode;
        if (controlsDict.TryGetValue("RIGHT", out rightKeycode))
        {
            keyRotateRight = rightKeycode;
        } 

        KeyCode reverseKeycode;
        if (controlsDict.TryGetValue("REVERSE", out reverseKeycode))
        {
            keyMoveReverse = reverseKeycode;
        } 
    }    

    public void Reset()
    {
        moveForward = false;
        moveReverse = false;
        moveSpeed = 1f;
        moveSpeedReverse = 0f;
        moveAcceleration = 0.1f;
        rotateRight = false;
        rotateLeft = false;
        rotateSpeedRight = 0f;
        rotateSpeedLeft = 0f;
        rotateAcceleration = 4f;
        rotateDeceleration = 10f;
        rotateSpeedMax = 130f;
    }
}