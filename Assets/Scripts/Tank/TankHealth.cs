﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Linq;   

public class TankHealth : MonoBehaviour
{
    public float m_StartingHealth = 100f;          
    public Image m_Healthbar;

    public GameObject m_HealthBarPrefab;
    public Color m_FullHealthColor = Color.green; 
    public Color m_MidHealthColor = Color.yellow; 
    public Color m_LowHealthColor = Color.red;
    public GameObject m_ExplosionAnimation;
    public float m_CurrentHealth;
    public bool m_Dead;

    void Start()
    {
        m_CurrentHealth = m_StartingHealth;
        m_Dead = false; 
    }

    void Update()
    {
        m_Dead = m_CurrentHealth <= 0;
        
        UpdateHealthBar();
    }

    /**
     * Appelée pour enlever des points de vie au tank
     */ 
    public void TakeDamage(float damage)
    {
        m_CurrentHealth -= damage;
        UpdateHealthBar();
        
        if (m_CurrentHealth <= 0)
        {
            m_Dead = true;
            this.OnDeath();
        }
    }

    /**
     * Appelée pour enlever des points de vie au tank, avec une référence au tireur
     */ 
    public void TakeDamage(float damage, Player player)
    {
        TakeDamage(damage);
        if(m_Dead)
        {
            player.nbKills++;
        }
    }

    private void InitializeHealthBar()
    {
        if (m_HealthBarPrefab == null)
        {
            m_HealthBarPrefab = Resources.Load("Prefabs/HealthBar") as GameObject;
        }        
        if (m_Healthbar == null)
        {
            GameObject healthBar = Instantiate(m_HealthBarPrefab, transform.position + new Vector3(0f, 3f, 0f), Quaternion.identity) as GameObject;
            healthBar.gameObject.transform.SetParent(gameObject.transform);
            m_Healthbar = healthBar.transform.Find("Background").GetComponent<Image>();
        }        
    }

    /**
     * Mise à jour de la barre de vie (UI)
     */ 
    private void UpdateHealthBar()
    {
        m_Healthbar.fillAmount = m_CurrentHealth/m_StartingHealth;

        if (m_CurrentHealth > 50f)
        {
            m_Healthbar.color = m_FullHealthColor;
        }
        else if (m_CurrentHealth > 20f)
        {
            m_Healthbar.color = m_MidHealthColor;
        }
        else
        {
            m_Healthbar.color = m_LowHealthColor;
        }
    }

    /**
     * Appellée lors de la mort du tank
     */ 
    private void OnDeath()
    {        
        SummonAnimation();
        List<IGameController> gameController = FindObjectsOfType<MonoBehaviour>().OfType<IGameController>().ToList();
        gameController[0].OnTankDead(gameObject);         
    }

    private void SummonAnimation()
    {
        GameObject explosion =
            Instantiate(m_ExplosionAnimation, transform.position, transform.rotation) as GameObject;
        explosion.GetComponent<ParticleSystem>().Play();
    }

    private void InstantDestroy()
    {
        Destroy(gameObject, 0);
    }
}