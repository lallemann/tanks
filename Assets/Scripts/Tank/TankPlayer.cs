using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankPlayer : MonoBehaviour
{

    public Player m_player {get; private set;}

    public GameObject m_NameTag;

    private TextMesh m_NameTagText;

    // Start is called before the first frame update
    void Start()
    {
        m_NameTag = transform.Find("TankRenderers/Nametag").GetComponent<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPlayer(Player player)
    {
        m_player = player;
        m_NameTag.GetComponent<Nametag>().playerId = player.Id;
        UpdateNameTag();
    }

    public void SetNameTagText(string text)
    {
        if (m_NameTagText != null)
        {
            m_NameTagText.text = text;
        }
    }

    private void UpdateNameTag()
    {
        if (m_NameTagText == null)
        {
            m_NameTagText = transform.Find("TankRenderers/Nametag").GetComponent<TextMesh>();
        }
        if (m_player != null)
        {
            SetNameTagText(m_player.Name);
        }   
    }
}
