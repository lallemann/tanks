using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankRespawn : MonoBehaviour
{

    /// <summary>
    /// Clone du gameObject du tank, en
    /// général cloné avant sa mort.
    /// </summary>
    public GameObject StateBeforeDeath { get; private set; }

    /// <summary>
    /// Nombre de vies (respawns) disponibles.
    /// </summary>
    public int LivesLeft = 0;

    /// <summary>
    /// Duplique le tank et le stocke dans StateBeforeDeath.
    /// A utiliser pour garder l'état du tank avant sa
    /// mort, pour pouvoir le faire respawner.
    /// </summary>
    public void SaveState()
    {
        this.StateBeforeDeath = Instantiate(gameObject);
    }
}
